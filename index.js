/**
 Since some requirements are interchangeable let group them and represent them with a unique numbers
 let Apartment, House, Flat = 1
 let property insurance = 2
 let 5 Door car, 4 door car, 3 door car, 2 door car = 3
 let Driving Licence = 4
 let Car Insureance = 5
 let Social Security Number = 6
 let Work Permit = 7
 let Bike, Scooter, Motorcycle  = 8
 let Motorcycle Insurance = 9
 let Massage Qualification Certificate = 10
 let Liability Insurance = 11
 let Storage Place, Garage = 12
 let PayPal account = 13
 */

//companies and requirements
let companies = {
  "Company A": [1, 2],
  "Company B": [3, 4, 5],
  "Company C": [6, 7],
  "Company D": [1],
  "Company E": [4, 3],
  "Company F": [8, 4, 9],
  "Company G": [10, 11],
  "Company H": [12],
  "Company J": [],
  "Company K": [13]
}

function canWorkAt(requirements){
  let firms = [];
  for(let company in companies){
    if(!companies[company].length){
      firms.push(company)
      continue
    }

    let result = requirements.every((val) => companies[company].indexOf(val) !== -1);

    if(result) firms.push(company)
  }

  return firms
}

function canNotWorkAt(requirements){
  let firms = [];
  for(let company in companies){
    if(!companies[company].length) continue
    
    let result = requirements.every((val) => companies[company].indexOf(val) !== -1);

    if(!result) firms.push(company)
    
  }

  return firms;
}



  


